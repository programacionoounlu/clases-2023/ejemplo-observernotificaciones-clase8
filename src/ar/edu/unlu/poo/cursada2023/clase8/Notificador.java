package ar.edu.unlu.poo.cursada2023.clase8;

import java.util.ArrayList;

public class Notificador implements Subject {
    ArrayList<Observer> observers = new ArrayList<>();
    String subjectState;

    @Override
    public void attach(Observer anObserver) {
        observers.add(anObserver);
    }

    @Override
    public void detach(Observer anObserver) {
        observers.remove(anObserver);
    }

    @Override
    public void notifyMessage() {
        for (Observer observer: observers) {
            observer.update();
        }
    }

    public void newMessage(String message) {
        subjectState = message;
        this.notifyMessage();
    }

    public String getState() {
        return subjectState;
    };
}
