package ar.edu.unlu.poo.cursada2023.clase8;

public class PatternMain {
    public static void main(String[] args) {
        Notificador notificadorApp = new Notificador();

        Observer client1 = new Client(notificadorApp, "1");
        notificadorApp.newMessage("Este es un mensaje que solo ve el cliente 1");

        Observer client2 = new Client(notificadorApp, "2");
        notificadorApp.newMessage("Este es un mensaje que ven los clientes 1 y 2");

        Observer client3 = new Client(notificadorApp, "3");
        notificadorApp.newMessage("Este es un mensaje que ven los 3 clientes");

        notificadorApp.detach(client2);
        notificadorApp.newMessage("Este es un mensaje que ven solo los clientes 1 y 3");

    }
}
