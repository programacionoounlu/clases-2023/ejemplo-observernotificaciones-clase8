package ar.edu.unlu.poo.cursada2023.clase8;

public interface Observer {
    public void update();
}
