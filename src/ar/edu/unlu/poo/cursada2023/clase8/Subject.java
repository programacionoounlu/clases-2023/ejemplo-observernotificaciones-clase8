package ar.edu.unlu.poo.cursada2023.clase8;

import java.util.ArrayList;

public interface Subject {
    public void attach(Observer anObserver);
    public void detach(Observer anObserver);
    public void notifyMessage();
}
