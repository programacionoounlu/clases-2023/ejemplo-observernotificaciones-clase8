package ar.edu.unlu.poo.cursada2023.clase8;

import java.util.ArrayList;

public class Client implements Observer {
    Subject subject;
    String observerState;
    String name;
    public Client(Subject aSubject, String aName) {
        subject = aSubject;
        name = aName;
        aSubject.attach(this);
    }
    @Override
    public void update() {
        observerState = ((Notificador) subject).getState();
        System.out.println("Soy el cliente " + name + " y el nuevo mensaje es " + observerState);
    }
}
